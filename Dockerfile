# To build
# docker image build -t theojust/theojust:privy-test .
# docker run -p 80:8000 -d theojust/theojust:privy-test
# docker tag theojust/theojust:privy-test asia.gcr.io/theojust/theojust:privy-test
# docker push asia.gcr.io/theojust/theojust:privy-test

FROM node:16.16.0-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
ENV PORT 8000
EXPOSE 8000
CMD ["npm", "run", "start"]